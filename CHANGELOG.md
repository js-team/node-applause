# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.0.3](https://github.com/outaTiME/applause/compare/v2.0.2...v2.0.3) (2021-05-03)


### Bug Fixes

* ignore non-string data types on replace ([bfbaa39](https://github.com/outaTiME/applause/commit/bfbaa39ba8123b9b8ab954b7964da0a831456ab8)), closes [#20](https://github.com/outaTiME/applause/issues/20)

### 2.0.2 (2021-05-03)


### Bug Fixes

* add type check before accesing String.replace ([93edf8e](https://github.com/outaTiME/applause/commit/93edf8ee3b0a36ab97797eb82061ecfd09ba97b3))

